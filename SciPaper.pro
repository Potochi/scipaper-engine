TEMPLATE = app
CONFIG += console
CONFIG -= qt

QMAKE_CFLAGS += -m32
QMAKE_LFLAGS += -m32
SOURCES += \
        data_structures/HashTable_meu.c \
        data_structures/LinkedList.c \
        data_structures/Queue.c \
        main.c \
        parson.c \
        publications.c \
        runner.c

HEADERS += \
  article.h \
  data_structures/DS_Settings.h \
  data_structures/HashTable_meu.h \
  data_structures/LinkedList.h \
  data_structures/Queue.h \
  libLivian.h \
  parson.h \
  publications.h \
  runner.h
