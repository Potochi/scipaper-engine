#ifndef ARTICLE_H
#define ARTICLE_H

#include <stdint.h>

typedef struct article_t {
  char* title;
  char* venue;
  int year;
  char** author_names;
  int64_t* author_ids;
  char** institutions;
  int num_authors;
  char** fields;
  int num_fields;
  int64_t id;
  int64_t* references;
  int num_refs;
  // index in array de structuri
  uint32_t index;
} article;

#endif  // ARTICLE_H
