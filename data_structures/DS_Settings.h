#ifndef DS_SETTINGS_H
#define DS_SETTINGS_H

typedef enum DS_Settings_T {
  FREE_NONE = 0,
  FREE_KEY = 0b0001,
  FREE_DATA = 0b0010,
  FREE_ALL = 0b0011,
} DS_Settings;

#endif  // DS_SETTINGS_H
