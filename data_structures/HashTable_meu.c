/* Copyright 2020 Bud Liviu + Banu Maximilian */

#ifndef HASHTABLE_MEU_C
#define HASHTABLE_MEU_C

#include "HashTable_meu.h"

void* ccalloc(size_t _nmemb, size_t _s) {
  void* ret = calloc(_nmemb, _s);
  if (ret == NULL) {
    perror("Failed to allocate memory!");
    exit(-1);
  }
  return ret;
}

void* cmalloc(size_t size) {
  void* ret = malloc(size);
  if (ret == NULL) {
    perror("Failed to allocate memory!");
    exit(-1);
  }
  return ret;
}
void* crealloc(void* _ptr, size_t _s) {
  void* ret = realloc(_ptr, _s);
  if (ret == NULL) {
    perror("Failed to reallocate memory!");
    exit(-1);
  }
  return ret;
}

int default_compare(void* first_key, void* second_key, size_t key_size) {
  return !memcmp(first_key, second_key, key_size);
}

size_t default_hash(void* raw_key, size_t key_size) {
  // djb2 string hashing algorithm
  // sstp://www.cse.yorku.ca/~oz/hash.ssml
  size_t byte;
  size_t hash = 5381;
  char* key = raw_key;

  for (byte = 0; byte < key_size; ++byte) {
    hash = ((hash << 5) + hash) ^ key[byte];
  }

  return hash;
}

void HTInit(HashTable* table, size_t k_size, size_t v_size, size_t capacity) {
  table->compare = default_compare;
  table->hash = default_hash;

  table->key_size = k_size;
  table->value_size = v_size;

  table->capacity = capacity;
  table->size = 0;

  table->entries = (HTEntry**)ccalloc(capacity, sizeof(HTEntry*));
}

size_t HTHash(HashTable* table, void* key) {
  return table->hash(key, table->key_size) % table->capacity;
}

void HTInsert(HashTable* table, void* key, void* value) {
  size_t index = HTHash(table, key);

  HTEntry* entry;

  for (entry = table->entries[index]; entry; entry = entry->next) {
    if (table->compare(key, entry->key, table->key_size)) {
      memcpy(entry->value, value, table->value_size);
      return;
    }
  }

  entry = _HTMakeEntry(table, key, value);

  entry->next = table->entries[index];
  table->entries[index] = entry;

  table->size++;
}

HTEntry* _HTMakeEntry(HashTable* table, void* key, void* value) {
  HTEntry* entry = (HTEntry*)cmalloc(sizeof(HTEntry));

  entry->key = cmalloc(table->key_size);
  entry->value = cmalloc(table->value_size);

  memcpy(entry->key, key, table->key_size);
  memcpy(entry->value, value, table->value_size);

  return entry;
}

int HTContains(HashTable* table, void* key) {
  size_t index = HTHash(table, key);
  HTEntry* entry;
  for (entry = table->entries[index]; entry; entry = entry->next) {
    if (table->compare(entry->key, key, table->key_size)) {
      return 1;
    }
  }
  return 0;
}

void HTFree(HashTable* table) {
  HTEntry* entry;
  HTEntry* tmp;

  size_t cap;

  for (cap = 0; cap < table->capacity; cap++) {
    entry = table->entries[cap];

    while (entry) {
      tmp = entry->next;

      free(entry->key);
      free(entry->value);
      free(entry);

      entry = tmp;
    }
  }

  free(table->entries);
}

void* HTGet(HashTable* table, void* key) {
  size_t index = HTHash(table, key);

  HTEntry* entry;
  for (entry = table->entries[index]; entry; entry = entry->next) {
    if (table->compare(key, entry->key, table->key_size)) {
      return entry->value;
    }
  }
  return NULL;
}

#endif  // HASHTABLE_MEU_C
