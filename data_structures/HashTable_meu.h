#ifndef HASHTABLE_MEU_H
#define HASHTABLE_MEU_H

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#include "string.h"

void* cmalloc(size_t size);

void* ccalloc(size_t _nmemb, size_t _s);

void* crealloc(void* _ptr, size_t _s);
#define HT_LOOKUP_AS(type, table_pointer, key_pointer) \
  (*(type*)HTGet((table_pointer), (key_pointer)))

typedef struct HTEntryT {
  struct HTEntryT* next;
  void* key;
  void* value;
} HTEntry;

typedef int (*compare_function)(void* val1, void* val2, size_t size);
typedef size_t (*hash_function)(void* val, size_t size);

typedef struct HasTableT {
  size_t key_size;
  size_t value_size;

  compare_function compare;
  hash_function hash;

  size_t size;
  size_t capacity;

  HTEntry** entries;

} HashTable;

void HTInit(HashTable* table, size_t k_size, size_t v_size, size_t capacity);
HTEntry* _HTMakeEntry(HashTable* table, void* key, void* value);
void HTInsert(HashTable* table, void* key, void* value);
int HTContains(HashTable* table, void* key);
void* HTGet(HashTable* table, void* key);
void HTFree(HashTable* table);

#endif  // HASHTABLE_MEU_H
