/* Copyright 2020 Bud Liviu */

#include "LinkedList.h"

#include <stdio.h>
#include <stdlib.h>

void add_node_position(LinkedList* list, int position, void* new_data) {
  if (position < 0) {
    return;
  }

  Node* new_node = malloc(sizeof(Node));
  new_node->next = NULL;
  new_node->prev = NULL;
  new_node->data = new_data;

  if (position == 0) {
    // Add head
    Node* old_head = list->head;
    list->head = new_node;
    new_node->next = old_head;
    if (old_head != NULL) {
      old_head->prev = new_node;
    }
    if (list->tail == NULL) {
      list->tail = new_node;
    }
  } else if (position == list->size) {
    // Add tail
    Node* old_tail = list->tail;
    list->tail = new_node;
    new_node->prev = old_tail;
    if (old_tail != NULL) {
      old_tail->next = new_node;
    }
  } else {
    Node* before = get_node(list, position - 1);
    Node* after = before->next;

    before->next = new_node;
    new_node->prev = before;

    after->prev = new_node;
    new_node->next = after;
  }
  list->size++;
}

Node* get_node(LinkedList* list, int position) {
  Node* current = list->head;
  for (int i = 0; i < position; i++) {
    current = current->next;
  }
  return current;
}

LinkedList* create_list() {
  LinkedList* list = malloc(sizeof(LinkedList));
  list->head = NULL;
  list->tail = NULL;
  list->size = 0;
  return list;
}

void init_list(LinkedList* list, DS_Settings settings) {
  list->settings = settings;
  list->head = NULL;
  list->tail = NULL;
  list->size = 0;
}

void remove_node_position(LinkedList* list, int position) {
  Node* to_remove = get_node(list, position);
  remove_node(list, to_remove);
}

void remove_node(LinkedList* list, Node* node) {
  if (node->next == NULL && node->prev == NULL) {
    // only node in list;
    list->head = NULL;
    list->tail = NULL;
  } else if (node->next == NULL) {
    // tail node
    list->tail = node->prev;
    list->tail->next = NULL;
  } else if (node->prev == NULL) {
    // head node
    list->head = node->next;
    list->head->prev = NULL;
  } else {
    Node* before = node->prev;
    Node* after = node->next;

    before->next = after;
    after->prev = before;
  }
  list->size--;
  if (list->settings & FREE_DATA) {
    free(node->data);
  }
  free(node);
}

void add_after(LinkedList* list, Node* node, void* new_data) {
  Node* new_node = malloc(sizeof(Node));
  new_node->next = NULL;
  new_node->prev = NULL;
  new_node->data = new_data;

  if (node == NULL) {
    // only node in list
    list->head = new_node;
    list->tail = new_node;
    return;
  }

  if (node->next == NULL) {
    // tail node
    node->next = new_node;
    new_node->prev = node;
    list->tail = new_node;

  } else {
    Node* after = node->next;

    node->next = new_node;
    new_node->prev = node;

    new_node->next = after;
    after->prev = new_node;
  }
  list->size++;
}

void free_list(LinkedList* list) {
  Node* head = list->head;
  Node* tmp;

  while (head != NULL) {
    tmp = head;
    head = head->next;
    if (list->settings & FREE_DATA) {
      free(tmp->data);
    }
    free(tmp);
  }
}
