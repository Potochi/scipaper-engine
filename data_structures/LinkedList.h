#ifndef LINKEDLIST_H
#define LINKEDLIST_H
// Copyright 2020 Bud Liviu

#include "DS_Settings.h"

typedef struct NodeT {
  struct NodeT* next;
  struct NodeT* prev;
  void* data;
} Node;

typedef struct LinkedListT {
  Node* head;
  Node* tail;
  int size;
  DS_Settings settings;
} LinkedList;

void add_node_position(LinkedList* list, int position, void* new_data);
void remove_node_position(LinkedList* list, int position);
void remove_node(LinkedList* list, Node* node);
void add_after(LinkedList* list, Node* node, void* new_data);
Node* get_node(LinkedList* list, int position);
void init_list(LinkedList* list, DS_Settings settings);
LinkedList* create_list(void);
void free_list(LinkedList* list);
#endif  // LINKEDLIST_H
