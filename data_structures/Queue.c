/* Copyright 2020 Bud Liviu + Banu Maximilian */

#include "Queue.h"

#include <stdio.h>
#include <stdlib.h>

void init_q(Queue *q, DS_Settings settings) {
  if (q == NULL) {
    perror("Not enough memory to initialize the queue!");
    exit(-1);
  }
  q->list = malloc(sizeof(LinkedList));
  q->settings = settings;
  init_list(q->list, settings);
}

int get_size_q(Queue *q) { return q->list->size; }

int is_empty_q(Queue *q) { return get_size_q(q) == 0; }

void *front(Queue *q) {
  if (q == NULL || q->list == NULL) {
    return NULL;
  }

  return q->list->head->data;
}

void dequeue(Queue *q) {
  if (q == NULL || q->list == NULL) {
    return;
  }
  remove_node_position(q->list, 0);
}

void enqueue(Queue *q, void *new_data) {
  add_node_position(q->list, q->list->size, new_data);
}

void clear_q(Queue *q) {
  while (!is_empty_q(q)) {
    remove_node_position(q->list, 0);
  }
}

void purge_q(Queue *q) {
  clear_q(q);
  free(q->list);
}
