#ifndef __QUEUE_H__
#define __QUEUE_H__

#include "DS_Settings.h"
#include "LinkedList.h"

typedef struct {
  LinkedList *list;
  DS_Settings settings;
} Queue;

void init_q(Queue *q, DS_Settings settings);

int get_size_q(Queue *q);

int is_empty_q(Queue *q);

void *front(Queue *q);

void dequeue(Queue *q);

void enqueue(Queue *q, void *new_data);

void clear_q(Queue *q);

void purge_q(Queue *q);

#endif /* __QUEUE_H__ */
