#ifndef LIBLIVIAN_H
#define LIBLIVIAN_H

#define COLOR_RESET "\033[0m"
#define COLOR_RED "\033[1;31m"
#define COLOR_YELLOW "\033[1;33m"
#define COLOR_CYAN "\033[1;36m"
#define COLOR_BLUE "\033[1;34m"
#define COLOR_GREEN "\033[1;32m"
#define COLOR_NONE ""

#define OUTPUT_BUFFER_SIZE 64000

#define LOG 1

#if LOG == 1
#define LOG_COLOR(color, format, ...) \
  printf("%s", color);                \
  printf(format, __VA_ARGS__);        \
  printf("%s", COLOR_RESET)
#else
#define LOG_COLOR(color, format, ...)
#endif

#define LOG_INFO(format, ...) LOG_COLOR(COLOR_YELLOW, format, __VA_ARGS__)

#define LOG_ERR(format, ...) LOG_COLOR(COLOR_RED, format, __VA_ARGS__)

#define LOG_GOOD(format, ...) LOG_COLOR(COLOR_GREEN, format, __VA_ARGS__)

#define LOG_TEXT(format, ...) LOG_COLOR(COLOR_NONE, format, __VA_ARGS__)
#if DEBUG_LOG == 1
#define LOG_DEBUG(format, ...) LOG_COLOR(COLOR_CYAN, format, __VA_ARGS__)
#else
#define LOG_DEBUG(format, ...)
#endif

#define ASSERT_FAIL(expected, got, eq)                                    \
  if (eq) {                                                               \
    LOG_ERR("ASSERTION FAIL!\n\t└──> \"%s\" == \"%s\"\n", expected, got); \
  } else {                                                                \
    LOG_ERR("ASSERTION FAIL!\n\t└──> \"%s\" != \"%s\"\n", expected, got); \
  }

#define ASSERT_EQ_STR(left, right) \
  if (strcmp(left, right)) {       \
    ASSERT_FAIL(left, right, 1);   \
  }

#define ASSERT_NEQ_STR(left, right) \
  if (!strcmp(left, right)) {       \
    ASSERT_FAIL(left, right, 0);    \
  }

#define START_TESTS()                                                  \
  {                                                                    \
    int __TESTS = 0;                                                   \
    int __TESTS_GOOD = 0;                                              \
    int __CURRENT_TEST_RESULT = 0;                                     \
    char* __OUTPUT_BUFFER = malloc(sizeof(char) * OUTPUT_BUFFER_SIZE); \
    memset(__OUTPUT_BUFFER, 0, OUTPUT_BUFFER_SIZE)

#define END_TESTS()                                                 \
  LOG_INFO("\nTests: %d  --  Passed: %d\n", __TESTS, __TESTS_GOOD); \
  if (__TESTS != __TESTS_GOOD) {                                    \
    LOG_ERR("%s\n", "TESTS FAILED!");                               \
  }                                                                 \
  free(__OUTPUT_BUFFER);                                            \
  }

#define _TEST_EQ(info, left, right)                    \
  LOG_TEXT("Testing %s...\t", info);                   \
  __TESTS++;                                           \
  __CURRENT_TEST_RESULT = strcmp(left, right) ? 0 : 1; \
  memset(__OUTPUT_BUFFER, 0, OUTPUT_BUFFER_SIZE);      \
  if (!__CURRENT_TEST_RESULT) {                        \
    LOG_ERR("%s\n", "Failed!");                        \
  } else {                                             \
    LOG_GOOD("%s\n", "Passed!");                       \
  }                                                    \
  if (!__CURRENT_TEST_RESULT) {                        \
    ASSERT_FAIL(left, right, 1);                       \
  }                                                    \
  __TESTS_GOOD += __CURRENT_TEST_RESULT;

#endif  // LIBLIVIAN_H
