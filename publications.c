/* Copyright 2020 Bud Liviu + Banu Maximilian */

#include "publications.h"

#include <stdio.h>
#include <string.h>

#include "article.h"
#include "data_structures/DS_Settings.h"
#include "data_structures/HashTable_meu.h"
#include "data_structures/LinkedList.h"
#include "data_structures/Queue.h"

typedef int64_t i64;
typedef uint64_t u64;

typedef int32_t i32;
typedef uint32_t u32;

struct publications_data {
  HashTable* articles;
  HashTable* citations;
  HashTable* venues;
  HashTable* authors;
  HashTable* fields;
  HashTable* institutions;
  i64* dates;
};

u32 hash_long(void* key) {
  u64 article_id = *(u64*)key;

  u32 x = article_id & 0x0000FFFF;
  x = ((x >> 16) ^ x) * 0x45d9f3b;
  x = ((x >> 16) ^ x) * 0x45d9f3b;
  x = (x >> 16) ^ x;
  return x;
}

uint32_t hash_str(const void* key1) {
  u32 h = 3323198485;
  char* key = (char*)key1;
  for (; *key; ++key) {
    h ^= *key;
    h *= 0x5bd1e995;
    h ^= h >> 15;
  }
  return h;
}

void free_paper(article* paper) {
  free(paper->title);
  free(paper->venue);
  free(paper->author_ids);
  free(paper->references);

  for (int i = 0; i < paper->num_authors; i++) {
    free(paper->author_names[i]);
  }
  free(paper->author_names);

  for (int i = 0; i < paper->num_authors; i++) {
    free(paper->institutions[i]);
  }
  free(paper->institutions);

  for (int i = 0; i < paper->num_fields; i++) {
    free(paper->fields[i]);
  }
  free(paper->fields);
}

typedef struct CitationsList_T {
  i64 citations_num;
  LinkedList* citations;
} CitationsList;

PublData* init_publ_data(void) {
  PublData* publ = (PublData*)cmalloc(sizeof(PublData));
  publ->articles = (HashTable*)cmalloc(sizeof(HashTable));
  publ->citations = (HashTable*)cmalloc(sizeof(HashTable));
  publ->venues = (HashTable*)cmalloc(sizeof(HashTable));
  publ->authors = (HashTable*)cmalloc(sizeof(HashTable));
  publ->fields = (HashTable*)cmalloc(sizeof(HashTable));
  publ->institutions = (HashTable*)cmalloc(sizeof(HashTable));

  HTInit(publ->articles, sizeof(i64), sizeof(article*), 50000);

  HTInit(publ->citations, sizeof(i64), sizeof(CitationsList*), 10000);

  HTInit(publ->venues, sizeof(u32), sizeof(LinkedList*), 5000);

  HTInit(publ->authors, sizeof(i64), sizeof(LinkedList*), 5000);

  HTInit(publ->fields, sizeof(u32), sizeof(LinkedList*), 5000);

  HTInit(publ->institutions, sizeof(u32), sizeof(HashTable*), 5000);

  publ->dates = (i64*)ccalloc(2021, sizeof(i64));

  return publ;
}
void destroy_publ_data(PublData* data) {
  free(data->dates);

  HashTable* citations = data->citations;

  for (size_t i = 0; i < citations->capacity; i++) {
    HTEntry* current = citations->entries[i];
    while (current != NULL) {
      CitationsList* list = *(CitationsList**)current->value;
      if (list != NULL) {
        free_list(list->citations);
        free(list->citations);
        free(list);
      }
      current = current->next;
    }
  }

  HTFree(citations);

  free(data->citations);

  HashTable* venues = data->venues;

  for (size_t i = 0; i < venues->capacity; i++) {
    HTEntry* current = venues->entries[i];
    while (current != NULL) {
      HTEntry* tmp = current;

      LinkedList* list = (LinkedList*)current->value;

      Node* list_node = list->head;

      while (list_node != NULL) {
        Node* tmpx;
        tmpx = list_node;
        list_node = list_node->next;
        free(tmpx);
      }

      current = tmp->next;
      free(tmp->key);
      free(tmp->value);
      free(tmp);
    }
  }
  free(venues->entries);
  free(venues);

  // Destroy papers;
  HashTable* articles = data->articles;

  for (size_t i = 0; i < articles->capacity; i++) {
    HTEntry* current = articles->entries[i];
    while (current != NULL) {
      article* art = *(article**)current->value;
      free_paper(art);
      free(art);
      current = current->next;
    }
  }

  HTFree(articles);

  free(data->articles);

  // Destroy authors;
  HashTable* authors = data->authors;

  for (size_t i = 0; i < authors->capacity; i++) {
    HTEntry* current = authors->entries[i];
    while (current != NULL) {
      LinkedList* list = *(LinkedList**)current->value;
      if (list != NULL) {
        free_list(list);
        free(list);
      }
      current = current->next;
    }
  }

  HTFree(authors);
  free(data->authors);

  HashTable* fields = data->fields;

  for (size_t i = 0; i < fields->capacity; i++) {
    HTEntry* current = fields->entries[i];
    while (current != NULL) {
      LinkedList* list = *(LinkedList**)current->value;
      if (list != NULL) {
        free_list(list);
        free(list);
      }
      current = current->next;
    }
  }

  HTFree(fields);
  free(data->fields);

  HashTable* institutions = data->institutions;

  for (size_t i = 0; i < institutions->capacity; i++) {
    HTEntry* current = institutions->entries[i];

    while (current != NULL) {
      HashTable* pog = *(HashTable**)current->value;

      for (size_t j = 0; j < pog->capacity; j++) {
        HTEntry* fi = pog->entries[j];

        while (fi != NULL) {
          HashTable* zoinks = *(HashTable**)fi->value;
          HTFree(zoinks);
          free(zoinks);
          fi = fi->next;
        }
      }
      HTFree(pog);
      current = current->next;
      free(pog);
    }
  }
  HTFree(institutions);
  free(institutions);
  free(data);
}

void add_paper(PublData* data, const char* title, const char* venue,
               const int year, const char** author_names,
               const int64_t* author_ids, const char** institutions,
               const int num_authors, const char** fields, const int num_fields,
               const int64_t id, const int64_t* references,
               const int num_refs) {
  // =====Adding paper in main hashtable=====

  article* new_article = (article*)ccalloc(1, sizeof(article));

  size_t title_len = strlen(title);
  size_t venue_len = strlen(venue);

  new_article->title = (char*)cmalloc(sizeof(char) * title_len + 1);
  new_article->venue = (char*)cmalloc(sizeof(char) * venue_len + 1);
  snprintf(new_article->title, title_len + 1, "%s", title);
  snprintf(new_article->venue, venue_len + 1, "%s", venue);

  new_article->year = year;
  new_article->num_authors = num_authors;
  new_article->num_fields = num_fields;
  new_article->id = id;
  new_article->num_refs = num_refs;

  new_article->author_ids = (i64*)cmalloc(sizeof(i64) * num_authors);
  memcpy(new_article->author_ids, author_ids, sizeof(i64) * num_authors);

  new_article->references = (i64*)cmalloc(sizeof(i64) * num_refs);
  memcpy(new_article->references, references, sizeof(i64) * num_refs);

  new_article->author_names = (char**)cmalloc(sizeof(char*) * num_authors);
  new_article->institutions = (char**)cmalloc(sizeof(char*) * num_authors);
  for (int i = 0; i < num_authors; i++) {
    size_t author_len = strlen(author_names[i]);
    size_t insti_len = strlen(institutions[i]);

    new_article->author_names[i] = cmalloc(sizeof(char) * author_len + 1);
    snprintf(new_article->author_names[i], author_len + 1, "%s",
             author_names[i]);

    new_article->institutions[i] = cmalloc(sizeof(char) * insti_len + 1);
    snprintf(new_article->institutions[i], insti_len + 1, "%s",
             institutions[i]);
  }

  new_article->fields = (char**)cmalloc(sizeof(char*) * num_fields);
  for (int i = 0; i < num_fields; i++) {
    size_t field_len = strlen(fields[i]);

    new_article->fields[i] = cmalloc(sizeof(char) * field_len + 1);
    snprintf(new_article->fields[i], field_len + 1, "%s", fields[i]);
  }

  HTInsert(data->articles, &new_article->id, &new_article);

  for (int i = 0; i < new_article->num_refs; i++) {
    if (HTContains(data->citations, &new_article->references[i])) {
      CitationsList* citations = HT_LOOKUP_AS(CitationsList*, data->citations,
                                              &new_article->references[i]);
      add_node_position(citations->citations, 0, &new_article->id);
      citations->citations_num++;
    } else {
      CitationsList* citations = (CitationsList*)cmalloc(sizeof(CitationsList));
      citations->citations = (LinkedList*)cmalloc(sizeof(LinkedList));
      init_list(citations->citations, FREE_NONE);
      HTInsert(data->citations, &new_article->references[i], &citations);
      citations->citations_num = 1;
      add_node_position(citations->citations, 0, &new_article->id);
    }
  }

  // ==================================================
  // =====Get venue impact factor====

  u32 venue_index = hash_str(new_article->venue);
  if (HTContains(data->venues, &venue_index)) {
    LinkedList* venue_list =
        HT_LOOKUP_AS(LinkedList*, data->venues, &venue_index);
    add_node_position(venue_list, 0, &new_article->id);
  } else {
    LinkedList* venue_list = (LinkedList*)cmalloc(sizeof(LinkedList));
    init_list(venue_list, FREE_NONE);
    HTInsert(data->venues, &venue_index, &venue_list);
    add_node_position(venue_list, 0, &new_article->id);
  }

  // Get erdos distance
  for (int i = 0; i < new_article->num_authors; i++) {
    if (HTContains(data->authors, &new_article->author_ids[i])) {
      LinkedList* published_by_author =
          HT_LOOKUP_AS(LinkedList*, data->authors, &new_article->author_ids[i]);
      add_node_position(published_by_author, 0, new_article);
    } else {
      LinkedList* published_by_author =
          (LinkedList*)cmalloc(sizeof(LinkedList));
      init_list(published_by_author, FREE_NONE);
      add_node_position(published_by_author, 0, new_article);
      HTInsert(data->authors, &new_article->author_ids[i],
               &published_by_author);
    }
  }

  // get paper between dates
  data->dates[new_article->year]++;

  for (int i = 0; i < new_article->num_fields; i++) {
    u32 field_index = hash_str(new_article->fields[i]);

    if (!HTContains(data->fields, &field_index)) {
      LinkedList* field_list = (LinkedList*)cmalloc(sizeof(LinkedList));
      init_list(field_list, FREE_NONE);

      HTInsert(data->fields, &field_index, &field_list);

      add_node_position(field_list, 0, new_article);
    } else {
      LinkedList* field_list =
          HT_LOOKUP_AS(LinkedList*, data->fields, &field_index);

      add_node_position(field_list, 0, new_article);
    }
  }

  // get authors with field

  for (int j = 0; j < new_article->num_fields; j++) {
    u32 field_id = hash_str(new_article->fields[j]);

    for (int i = 0; i < num_authors; i++) {
      u32 insti_id = hash_str(new_article->institutions[i]);
      i64 author_id = new_article->author_ids[i];
      HashTable* instis;

      if (HTContains(data->institutions, &insti_id)) {
        instis = HT_LOOKUP_AS(HashTable*, data->institutions, &insti_id);
      } else {
        instis = (HashTable*)cmalloc(sizeof(HashTable));
        HTInit(instis, sizeof(u32), sizeof(HashTable*), 50);
        HTInsert(data->institutions, &insti_id, &instis);
      }
      if (HTContains(instis, &field_id)) {
        HashTable* fieldss = HT_LOOKUP_AS(HashTable*, instis, &field_id);
        HTInsert(fieldss, &author_id, &author_id);
      } else {
        HashTable* fieldss = (HashTable*)cmalloc(sizeof(HashTable));
        HTInit(fieldss, sizeof(i64), sizeof(i64), 50);
        HTInsert(instis, &field_id, &fieldss);
        HTInsert(fieldss, &author_id, &author_id);
      }
    }
  }
}

char* get_oldest_influence(PublData* data, const int64_t id_paper) {
  article* oldest_paper = NULL;

  HashTable* articles = data->articles;
  i64 id_paper_copy = id_paper;

  article* root = HT_LOOKUP_AS(article*, articles, &id_paper_copy);
  HashTable* visited = (HashTable*)cmalloc(sizeof(HashTable));
  HTInit(visited, sizeof(i64), sizeof(article*), 2000);

  Queue* bfs_queue = (Queue*)cmalloc(sizeof(Queue));
  init_q(bfs_queue, FREE_NONE);

  enqueue(bfs_queue, root);

  while (!is_empty_q(bfs_queue)) {
    article* paper = (article*)front(bfs_queue);
    HTInsert(visited, &paper->id, &paper);

    for (int i = 0; i < paper->num_refs; i++) {
      if (HTContains(articles, &paper->references[i]) &&
          !HTContains(visited, &paper->references[i])) {
        enqueue(bfs_queue,
                HT_LOOKUP_AS(article*, articles, &paper->references[i]));
        HTInsert(visited, &paper->references[i], &paper);
      }
    }

    if (oldest_paper == NULL && strcmp(root->title, paper->title)) {
      oldest_paper = paper;
    }
    if (oldest_paper != NULL) {
      if (paper->year < oldest_paper->year) {
        oldest_paper = paper;
      }
      if (paper->year == oldest_paper->year) {
        i64* paper_citations =
            &(HT_LOOKUP_AS(CitationsList*, data->citations, &paper->id)
                  ->citations_num);
        i64* oldest_citations =
            &(HT_LOOKUP_AS(CitationsList*, data->citations, &oldest_paper->id)
                  ->citations_num);
        if (*paper_citations > *oldest_citations) {
          oldest_paper = paper;
        } else if (*paper_citations == *oldest_citations) {
          if (paper->id < oldest_paper->id) {
            oldest_paper = paper;
          }
        }
      }
    }
    dequeue(bfs_queue);
  }
  purge_q(bfs_queue);
  free(bfs_queue);
  HTFree(visited);
  free(visited);

  if (oldest_paper != NULL) {
    return oldest_paper->title;
  } else {
    return "None";
  }
}

float get_venue_impact_factor(PublData* data, const char* venue) {
  u32 venue_index = hash_str(venue);
  double citations = 0;
  double articles = 0;

  LinkedList* venue_list =
      HT_LOOKUP_AS(LinkedList*, data->venues, &venue_index);

  Node* current = venue_list->head;

  while (current) {
    i64 current_id = *(i64*)current->data;
    if (HTContains(data->articles, &current_id)) {
      if (HTContains(data->citations, &current_id)) {
        CitationsList* citations_list =
            HT_LOOKUP_AS(CitationsList*, data->citations, &current_id);
        citations += citations_list->citations_num;
      }
    }
    articles++;
    current = current->next;
  }

  return citations / articles;
}

int get_number_of_influenced_papers(PublData* data, const int64_t id_paper,
                                    const int distance) {
  i64 id_copy = id_paper;

  if (!HTContains(data->citations, &id_copy)) {
    return 0;
  }

  HashTable* visited = (HashTable*)cmalloc(sizeof(HashTable));
  HTInit(visited, sizeof(i64), sizeof(i64), 2000);

  int influenced_papers = 0;
  int current_distance = 0;

  Queue* bfs_queue = (Queue*)cmalloc(sizeof(Queue));
  init_q(bfs_queue, FREE_NONE);

  enqueue(bfs_queue, &id_copy);
  enqueue(bfs_queue, NULL);
  HTInsert(visited, &id_copy, &id_copy);

  while (!is_empty_q(bfs_queue)) {
    if (front(bfs_queue) == NULL) {
      current_distance++;
      enqueue(bfs_queue, NULL);
      dequeue(bfs_queue);
    }

    if (front(bfs_queue) == NULL) {
      break;
    }

    if (current_distance >= distance) {
      break;
    }

    i64 root = *(i64*)front(bfs_queue);

    if (HTContains(data->citations, &root)) {
      CitationsList* cited_by =
          HT_LOOKUP_AS(CitationsList*, data->citations, &root);

      Node* cited = cited_by->citations->head;

      while (cited != NULL) {
        if (!HTContains(visited, (i64*)cited->data)) {
          enqueue(bfs_queue, (i64*)cited->data);
          HTInsert(visited, (i64*)cited->data, (i64*)cited->data);
          influenced_papers++;
        }
        cited = cited->next;
      }
    }

    dequeue(bfs_queue);
  }

  purge_q(bfs_queue);
  free(bfs_queue);
  HTFree(visited);
  free(visited);

  return influenced_papers;
}

int get_erdos_distance(PublData* data, const int64_t id1, const int64_t id2) {
  i64 id1_copy = id1;
  int erdos_distance = 0;

  HashTable* visited = (HashTable*)cmalloc(sizeof(HashTable));
  HTInit(visited, sizeof(i64), sizeof(i64), 1000);

  Queue* bfs_queue = (Queue*)cmalloc(sizeof(Queue));
  init_q(bfs_queue, FREE_NONE);

  enqueue(bfs_queue, &id1_copy);
  enqueue(bfs_queue, NULL);

  while (bfs_queue != NULL) {
    if (front(bfs_queue) == NULL) {
      erdos_distance++;
      enqueue(bfs_queue, NULL);
      dequeue(bfs_queue);
    }

    if (front(bfs_queue) == NULL) {
      break;
    }

    i64 root = *(i64*)front(bfs_queue);
    if (HTContains(data->authors, &root)) {
      LinkedList* published_by_root =
          HT_LOOKUP_AS(LinkedList*, data->authors, &root);

      Node* current = published_by_root->head;
      while (current != NULL) {
        article* current_article = (article*)current->data;
        for (int i = 0; i < current_article->num_authors; i++) {
          if (current_article->author_ids[i] == id2) {
            HTFree(visited);
            free(visited);
            purge_q(bfs_queue);
            free(bfs_queue);

            return erdos_distance + 1;
          } else {
            if (!HTContains(visited, &current_article->author_ids[i])) {
              enqueue(bfs_queue, &current_article->author_ids[i]);
              HTInsert(visited, &current_article->author_ids[i],
                       &current_article->author_ids[i]);
            }
          }
        }
        current = current->next;
      }
    }
    dequeue(bfs_queue);
  }
  HTFree(visited);
  free(visited);
  purge_q(bfs_queue);
  free(bfs_queue);

  return -1;
}

typedef struct pog_T {
  article* art;
  i64 n_cit;
} pog;

int pog_compare(const void* pog1, const void* pog2) {
  pog* elem1 = (pog*)pog1;
  pog* elem2 = (pog*)pog2;
  int a;
  if ((a = elem2->n_cit - elem1->n_cit)) {
    return a;
  }
  if ((a = elem2->art->year - elem1->art->year)) {
    return a;
  }
  return (elem1->art->id - elem2->art->id);
}

char** get_most_cited_papers_by_field(PublData* data, const char* field,
                                      int* num_papers) {
  char field_copy[300] = {0};
  snprintf(field_copy, sizeof(field_copy), "%s", field);

  u32 field_index = hash_str(field_copy);

  if (HTContains(data->fields, &field_index)) {
    LinkedList* papers_in_field =
        HT_LOOKUP_AS(LinkedList*, data->fields, &field_index);
    if (papers_in_field->size < *num_papers) {
      *num_papers = papers_in_field->size;
    }
    pog* poggers = (pog*)ccalloc(papers_in_field->size, sizeof(pog));

    Node* iterator = papers_in_field->head;
    int index = 0;
    while (iterator != NULL) {
      article* art = (article*)iterator->data;
      poggers[index].art = art;
      if (HTContains(data->citations, &art->id)) {
        CitationsList* citations =
            HT_LOOKUP_AS(CitationsList*, data->citations, &art->id);
        poggers[index].n_cit = citations->citations_num;
      } else {
        poggers[index].n_cit = 0;
      }
      index++;
      iterator = iterator->next;
    }
    char** ret_val = (char**)cmalloc(sizeof(char*) * (*num_papers));
    qsort(poggers, papers_in_field->size, sizeof(pog), pog_compare);
    for (int i = 0; i < *num_papers; i++) {
      size_t pog_len = strlen(poggers[i].art->title);

      ret_val[i] = (char*)cmalloc(sizeof(char) * pog_len + 1);
      snprintf(ret_val[i], pog_len + 1, "%s", poggers[i].art->title);
    }
    free(poggers);
    return ret_val;
  }
  *num_papers = 0;
  return NULL;
}

int get_number_of_papers_between_dates(PublData* data, const int early_date,
                                       const int late_date) {
  int number = 0;
  for (int i = early_date; i <= late_date; i++) {
    number += data->dates[i];
  }
  return number;
}

int get_number_of_authors_with_field(PublData* data, const char* institution,
                                     const char* field) {
  u32 insti_id = hash_str(institution);
  u32 field_id = hash_str(field);

  if (HTContains(data->institutions, &insti_id)) {
    HashTable* fields = HT_LOOKUP_AS(HashTable*, data->institutions, &insti_id);

    if (HTContains(fields, &field_id)) {
      HashTable* authors = HT_LOOKUP_AS(HashTable*, fields, &field_id);
      return authors->size;
    }
  }

  return 0;
}

int* get_histogram_of_citations(PublData* data, const int64_t id_author,
                                int* num_years) {
  i64 id_copy = id_author;
  int min_year = 200000;
  int* histogram = (int*)ccalloc(2000, sizeof(int));
  LinkedList* article_list = HT_LOOKUP_AS(LinkedList*, data->authors, &id_copy);

  Node* current = article_list->head;

  while (current != NULL) {
    article* cur_article = (article*)current->data;
    if (cur_article->year < min_year) {
      min_year = cur_article->year;
    }
    if (HTContains(data->citations, &cur_article->id)) {
      CitationsList* citations =
          HT_LOOKUP_AS(CitationsList*, data->citations, &cur_article->id);
      histogram[2020 - cur_article->year] += citations->citations_num;
    }
    current = current->next;
  }
  *num_years = 2020 - min_year + 1;

  return histogram;
}

char** get_reading_order(PublData* data, const int64_t id_paper,
                         const int distance, int* num_papers) {
  (void)data;
  (void)id_paper;
  (void)distance;
  (void)num_papers;

  return NULL;
}
char* find_best_coordinator(PublData* data, const int64_t id_author) {
  (void)data;
  (void)id_author;

  return NULL;
}
